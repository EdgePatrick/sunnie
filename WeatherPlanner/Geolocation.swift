//
//  Geolocation.swift
//  WeatherPlanner
//
//  Created by Patrick Edge on 26.4.17.
//  Copyright © 2017 Patrik Roh. All rights reserved.
//


import Foundation
import CoreLocation

class Geolocation {
    
    func forwardGeocoding(_ address: String, completion: @escaping (CLLocation?, NSError?) -> Void) {
        
        CLGeocoder().geocodeAddressString(address, completionHandler: { (placemarks, error) in
            
            if error != nil {
                completion(nil, error! as NSError?)
            } else {
                if let placemarks = placemarks , !placemarks.isEmpty {
                    let placemark = placemarks.first!
                    if let unwrappedLocation = placemark.location {
                        let coordinate = unwrappedLocation.coordinate
                        print("Location: \(coordinate.latitude), \(coordinate.longitude)")
                        let CLReadyLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                        completion(CLReadyLocation, nil)
                    }
                }
            }
        })
    }
    
    func reverseGeocoding(_ location: CLLocation, completion: @escaping (CLPlacemark?, NSError?) -> Void) {
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
            
            if error != nil {
                completion(nil, error! as NSError?)
            } else {
                if let placemarks = placemarks , !placemarks.isEmpty {
                    let placemark = placemarks.first!
                    completion(placemark, nil)
                    
                }
            }
        })
    }
    
}
