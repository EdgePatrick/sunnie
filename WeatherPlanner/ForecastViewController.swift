//
//  ForecastViewController.swift
//  WeatherPlanner
//
//  Created by Patrik Roh on 05/05/2017.
//  Copyright © 2017 Patrik Roh. All rights reserved.
//

import UIKit

class ForecastViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    private let openWeatherMapAPIKey = "1ba290c9e3245117975e51c0b99c6de6"
    var weatherForecast: [WeatherForecast] = []
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let bounds = UIScreen.main.bounds
        let width = bounds.size.width
        let height = bounds.size.height
        
        let background = Background(width: width, height: height)
        let layer = background.getBackgroundLayerBlue()
        view.layer.insertSublayer(layer, at: 0)
        
        tableView.backgroundColor = .clear
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        
        if let forecast = appDelegate.forecast{
            self.weatherForecast = forecast
        }
        
        
    }

    // MARK: tableView setup
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherForecast.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "forecastCell", for: indexPath) as! ForecastTableViewCell
        cell.day.text = weatherForecast[indexPath.row].date
        cell.temperature.text = "\(weatherForecast[indexPath.row].temperature!)°C"
        cell.icon.image = weatherForecast[indexPath.row].icon
        return cell
    }
    

}
