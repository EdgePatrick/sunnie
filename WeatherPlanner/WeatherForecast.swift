//
//  WeatherForecast.swift
//  WeatherPlanner
//
//  Created by Patrick Edge on 14.4.17.
//  Copyright © 2017 Patrik Roh. All rights reserved.
//

import UIKit

class WeatherForecast {
    
    var date: String?
    var detaileddate: String?
    let temperature: Int?
    let weatherId: Int?
    let weatherDescription: String?
    let pressure: Int?
    let humidity: Int?
    let windSpeed: Int?
    let clouds: Int?
    var icon = UIImage(named: "default.png")
    
    let dateFormatter = DateFormatter()
    
    init(forecastDict: [String:AnyObject]) {
        let weatherArray = forecastDict["weather"] as? [[String:AnyObject]]
        let weatherDict = weatherArray?.first

        self.temperature = forecastDict["temp"]?["day"] as? Int
        self.weatherId = weatherDict?["id"] as? Int
        self.weatherDescription = weatherDict?["main"] as? String
        self.pressure = forecastDict["pressure"] as? Int
        self.humidity = forecastDict["humidity"] as? Int
        self.windSpeed = forecastDict["speed"] as? Int
        self.clouds = forecastDict["clouds"] as? Int
        
        if let icon = weatherDict?["icon"] as? String {
            self.icon = UIImage(named: "\(icon).png")
        }
        
        if let date = forecastDict["dt"] as? Double {
            self.date = self.simpleStringFromUnixTime(unixTime: date)
        } else {
            self.date = nil
        }
        
        if let date = forecastDict["dt"] as? Double {
            self.detaileddate = self.detailedStringFromUnixTime(unixTime: date)
        } else {
            self.detaileddate = nil
        }
        

    }
    
    func simpleStringFromUnixTime(unixTime: Double) -> String {
        let date = NSDate(timeIntervalSince1970: unixTime)
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: date as Date)
    }
    
    func detailedStringFromUnixTime(unixTime: Double) -> String {
        let date = NSDate(timeIntervalSince1970: unixTime)
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "EEEE d. M."
        return dateFormatter.string(from: date as Date)
    }
    
    /*
    required init?(coder aDecoder: NSCoder) {
        
        self.date = aDecoder.decodeObject(forKey: "date") as? String
        self.detaileddate = aDecoder.decodeObject(forKey: "detaileddate") as? String
        self.temperature = aDecoder.decodeObject(forKey: "temperature") as? Int
        self.weatherId = aDecoder.decodeObject(forKey: "weatherId") as? Int
        self.weatherDescription = aDecoder.decodeObject(forKey: "weatherDescription") as? String
        self.pressure = aDecoder.decodeObject(forKey: "pressure") as? Int
        self.humidity = aDecoder.decodeObject(forKey: "humidity") as? Int
        self.windSpeed = aDecoder.decodeObject(forKey: "windSpeed") as? Int
        self.clouds = aDecoder.decodeObject(forKey: "clouds") as? Int
        self.icon = aDecoder.decodeObject(forKey: "icon") as? UIImage
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.date, forKey: "date")
        aCoder.encode(self.date, forKey: "detaileddate")
        aCoder.encode(self.date, forKey: "temperature")
        aCoder.encode(self.date, forKey: "weatherId")
        aCoder.encode(self.date, forKey: "weatherDescription")
        aCoder.encode(self.date, forKey: "pressure")
        aCoder.encode(self.date, forKey: "humidity")
        aCoder.encode(self.date, forKey: "windSpeed")
        aCoder.encode(self.date, forKey: "clouds")
        aCoder.encode(self.date, forKey: "icon")
    }
    */
    
    
    
}
