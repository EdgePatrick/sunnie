//
//  SectionsData.swift
//  WeatherPlanner
//
//  Created by Patrik Roh on 04/05/2017.
//  Copyright © 2017 Patrik Roh. All rights reserved.
//

import Foundation


/*
 
 var cityArray: [String] = []
 var forecastDict: [String:[WeatherForecast]] = [:]
 var topDaysDict: [String:[Int]] = [:]
 
 */

class SectionsData {
    
    func getSectionsData(cityArray: [String], forecastDict: [String:[WeatherForecast]], topDaysDict: [String:[Int]]) -> [Sections] {
    
        var sectionsArray:[Sections] = []
        
        for city in cityArray {
            let object = Sections(title: city, items: getWeatherObjects(city: city, forecastDict: forecastDict, topDaysDict: topDaysDict))
            sectionsArray.append(object)
        }
        return sectionsArray
        
    }
    
    func getWeatherObjects(city: String, forecastDict: [String:[WeatherForecast]], topDaysDict: [String:[Int]]) -> [WeatherForecast] {
        
        let forecastForCity = forecastDict[city]
        let daysForCity = topDaysDict[city]
        var restultArray: [WeatherForecast] = []
        
        for day in daysForCity!{
            restultArray.append(forecastForCity![day])
        }
        return restultArray
        
    }
    
    
    
    
    
    
}
