//
//  AlertController.swift
//  WeatherPlanner
//
//  Created by Patrick Edge on 26.4.17.
//  Copyright © 2017 Patrik Roh. All rights reserved.
//

import UIKit

class AlertController {
    
    func enterCityAlert() -> UIAlertController {
        let alertController = UIAlertController(title: "Add new city", message: "", preferredStyle: .alert)
        
        let enterAction = UIAlertAction(title: "Enter", style: .default, handler: {
            alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            print(firstTextField.text)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = ""
        }
        
        alertController.addAction(enterAction)
        alertController.addAction(cancelAction)
        
        return alertController
        
    }
    
    
    
    
    
    
    
    
}
