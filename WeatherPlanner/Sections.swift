//
//  Sections.swift
//  WeatherPlanner
//
//  Created by Patrik Roh on 04/05/2017.
//  Copyright © 2017 Patrik Roh. All rights reserved.
//

import Foundation

class Sections {
    
    let title: String
    let items: [WeatherForecast]
    
    init(title: String, items: [WeatherForecast]) {
        self.title = title
        self.items = items
    }
}
