//
//  Extensions.swift
//  WeatherPlanner
//
//  Created by Patrick Edge on 21.4.17.
//  Copyright © 2017 Patrik Roh. All rights reserved.
//

import UIKit

extension Date {
    func getTodaysDate() -> (day: String, date: String) {
        
        let currentDate = Date()
        let monthFormatter = DateFormatter()
        let dayFormatter = DateFormatter()
        
        monthFormatter.dateFormat = "MMMM"
        monthFormatter.locale = Locale(identifier: "en_US")
        let month = monthFormatter.string(from: currentDate)
        
        dayFormatter.dateFormat = "EEEE"
        dayFormatter.locale = Locale(identifier: "en_US")
        let dayToday = dayFormatter.string(from: currentDate)
        
        let calendar = Calendar.current
        let day = calendar.component(.day, from: currentDate)
        
        let date = ("\(month) \(day)")

        return (day: dayToday.uppercased(), date: date.uppercased())
        
    }
}

extension String {
    
    func removeWhitespace() -> String {
        return self.replacingOccurrences(of: " ", with: "")
    }
}

extension Array {
    func indexesOf<T : Equatable>(object:T) -> [Int] {
        var result: [Int] = []
        for (index,obj) in enumerated() {
            if obj as! T == object {
                result.append(index)
            }
        }
        return result
    }
}

public extension DispatchQueue {
    private static var _onceTracker = [String]()
    
    public class func once(file: String = #file, function: String = #function, line: Int = #line, block:(Void)->Void) {
        let token = file + ":" + function + ":" + String(line)
        once(token: token, block: block)
    }
    
    /**
     Executes a block of code, associated with a unique token, only once.  The code is thread safe and will
     only execute the code once even in the presence of multithreaded calls.
     
     - parameter token: A unique reverse DNS style name such as com.vectorform.<name> or a GUID
     - parameter block: Block to execute once
     */
    public class func once(token: String, block:(Void)->Void) {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        
        
        if _onceTracker.contains(token) {
            return
        }
        
        _onceTracker.append(token)
        block()
    }
}
