//
//  PlanATripViewController.swift
//  WeatherPlanner
//
//  Created by Patrick Edge on 24.4.17.
//  Copyright © 2017 Patrik Roh. All rights reserved.
//

import UIKit

class PlanATripViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var leftBarButtonInem: UIBarButtonItem!
    let geolocation = Geolocation()
    private let openWeatherMapAPIKey = "1ba290c9e3245117975e51c0b99c6de6"
    
    var cityArray: [String] = []
    var forecastDict: [String:[WeatherForecast]] = [:]
    var topDaysDict: [String:[Int]] = [:]

    // MARK: button methods
    
    @IBAction func addCity(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Add new city", message: "", preferredStyle: .alert)
        
        let enterAction = UIAlertAction(title: "Enter", style: .default, handler: {
            alert -> Void in
            let textField = alertController.textFields![0] as UITextField
            
            self.geolocation.forwardGeocoding(textField.text!, completion: { (location, error) in
                if error != nil {
                    print("error")
                } else {
                    if let location = location {
                        print(location)
                        self.tableView.beginUpdates()
                        self.cityArray.append(textField.text!.capitalized)
                        self.tableView.insertRows(at: [IndexPath(row: self.cityArray.count-1, section: 0)], with: .automatic)
                        self.tableView.endUpdates()
                    }
                }
            })
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = ""
        }
        alertController.addAction(enterAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func planATrip(_ sender: Any) {
        print("Plan a trip")
        let forecastService = ForecastService(APIKey: openWeatherMapAPIKey)
        let myGroup = DispatchGroup()
        let score = CityScore()
        for city in cityArray {
            myGroup.enter()
            forecastService.getWeatherForecast(city: city.removeWhitespace(), completion: { (forecast) in
                if let currentForecast = forecast?.weatherForecastArray {
                    //self.forecastArray = currentForecast
                    print("\(city),\(score.compute(weatherArray: currentForecast).indexes)")
                    self.forecastDict[city] = currentForecast
                    self.topDaysDict[city] = score.compute(weatherArray: currentForecast).indexes
                    myGroup.leave()
                }
            })
            
        }
        myGroup.notify(queue: .main) {
            self.performSegue(withIdentifier: "push", sender: nil)
        }
    }
    
    
    @IBAction func editCities(_ sender: Any) {
        
        if tableView.isEditing{
            tableView.setEditing(false, animated: true)
            leftBarButtonInem.title = "Edit"
        } else {
            tableView.setEditing(true, animated: true)
            leftBarButtonInem.title = "Done"
        }
    }
    
    // MARK: tableView setup
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "inputCell", for: indexPath)
        
        cell.textLabel?.text = cityArray[indexPath.row]
        cell.textLabel?.textColor = .white
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.cityArray.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let rowToMove = cityArray[sourceIndexPath.row]
        cityArray.remove(at: sourceIndexPath.row)
        cityArray.insert(rowToMove, at: destinationIndexPath.row)
        print(cityArray)
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // MARK: view lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width
        let height = bounds.size.height
        
        let background = Background(width: width, height: height)
        let layer = background.getBackgroundLayerBlue()
        view.layer.insertSublayer(layer, at: 0)
        
        tableView.backgroundColor = .clear
        
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    // MARK: prepare for segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "push" {
            let destination = segue.destination as! ResultViewController
            destination.cityArray = self.cityArray
            destination.forecastDict = self.forecastDict
            destination.topDaysDict = self.topDaysDict
        }
    }

}
