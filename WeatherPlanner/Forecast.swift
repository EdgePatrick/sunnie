//
//  Forecast.swift
//  WeatherPlanner
//
//  Created by Patrick Edge on 14.4.17.
//  Copyright © 2017 Patrik Roh. All rights reserved.
//

import Foundation

struct Forecast { //Wrapper
    
    var weatherForecastArray: [WeatherForecast] = []

    init(weatherDictionary: [String:AnyObject]?) {
        
        if let weatherForecastArray = weatherDictionary?["list"] as? [[String:AnyObject]]{
            
            for dailyWeather in weatherForecastArray{
                let forecast = WeatherForecast(forecastDict: dailyWeather)
                self.weatherForecastArray.append(forecast)
            }
            
        }
        
        
    }
    
}
