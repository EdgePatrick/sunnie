//
//  Background.swift
//  WeatherPlanner
//
//  Created by Patrick Edge on 21.4.17.
//  Copyright © 2017 Patrik Roh. All rights reserved.
//

import UIKit

class Background {
    
    let width: CGFloat?
    let height: CGFloat?
    
    init(width: CGFloat, height: CGFloat) {
        self.width = width
        self.height = height
    }
    
    func getBackgroundLayerBlue() -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = CGRect(x: 0, y: 0, width: width!, height: height!)
        
        let lowerColor = UIColor(red: 59/255, green: 153/255, blue: 219/255, alpha: 1)
        let upperColor = UIColor(red: 14/255, green: 74/255, blue: 157/255, alpha: 1)
        layer.colors = [upperColor.cgColor, lowerColor.cgColor]
        
        return layer
    }
    
    func getBackgroundLayerLightBlue() -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = CGRect(x: 0, y: 0, width: width!, height: height!)
        
        let lowerColor = UIColor(red: 59/255, green: 153/255, blue: 219/255, alpha: 1)
        let upperColor = UIColor(red: 58/255, green: 123/255, blue: 213/255, alpha: 1)
        layer.colors = [upperColor.cgColor, lowerColor.cgColor]
        
        return layer
    }
    
    
}
