//
//  NetworkOperation.swift
//  WeatherPlanner
//
//  Created by Patrick Edge on 9.4.17.
//  Copyright © 2017 Patrik Roh. All rights reserved.
//

import Foundation

class NetworkOperation {
    
    lazy var config: URLSessionConfiguration = URLSessionConfiguration.default
    lazy var session: URLSession = URLSession(configuration: self.config)
    
    typealias JSONDictionaryCompletion = ([String:AnyObject]?) -> Void
    
    let queryURL: NSURL

    init(url: NSURL) {
        self.queryURL = url
    }
    
    func downloadJSONFromURL(completion: @escaping JSONDictionaryCompletion){
        let request: URLRequest = URLRequest(url: queryURL as URL)
        let dataTask = session.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            //print("background thread")
            // 1. Check HTTP response for successful GET request
            if let httpResponse = response as? HTTPURLResponse{ //urlresponse nemá status code, preto ho castujem na httpresponse
                switch(httpResponse.statusCode){
                case 200: // 2. Create JSON with data
                    do {
                        let jsonDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:AnyObject]
                        completion(jsonDictionary)
                    } catch {
                        print(error)
                    }
                    
                default: print("Get request fail, status \(httpResponse.statusCode)")
                }
                
            } else {
                print("not a valid http response") //handle this better (popup?)
            }
            
        }
        dataTask.resume()
        //print("main thread")
    }
    
    
    
    
    
    
    
    
    
    
    
}
