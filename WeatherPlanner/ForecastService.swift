//
//  ForecastService.swift
//  WeatherPlanner
//
//  Created by Patrick Edge on 9.4.17.
//  Copyright © 2017 Patrik Roh. All rights reserved.
//

import Foundation

struct ForecastService {
    
    let forecastAPIKey: String
    let forecastBaseURL: URL?
    
    init(APIKey: String){
        self.forecastAPIKey = APIKey
        self.forecastBaseURL = URL(string: "http://api.openweathermap.org/data/2.5/")
    }
    
    
    func getWeatherForecast(city: String, completion: @escaping ((Forecast?) -> Void)) {
        if let forecastURL = URL(string: "forecast/daily?q=\(city)&appid=\(forecastAPIKey)&units=metric&cnt=16", relativeTo: forecastBaseURL) {
                let networkOperation = NetworkOperation(url: forecastURL as NSURL)
                networkOperation.downloadJSONFromURL(completion: { (JSONDictionary) in
                    let forecast = Forecast(weatherDictionary: JSONDictionary)
                    completion(forecast)
                })
        }
    }
    


}
