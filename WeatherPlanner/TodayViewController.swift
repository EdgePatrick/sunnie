//
//  ViewController.swift
//  WeatherPlanner
//
//  Created by Patrick Edge on 28.2.17.
//  Copyright © 2017 Patrik Roh. All rights reserved.
//

import UIKit

class TodayViewController: UIViewController {

    private let openWeatherMapAPIKey = "1ba290c9e3245117975e51c0b99c6de6"
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    
    @IBOutlet weak var windSpeedLabel: UILabel!
    
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width
        let height = bounds.size.height
        
        let background = Background(width: width, height: height)
        let layer = background.getBackgroundLayerBlue()
        view.layer.insertSublayer(layer, at: 0)
        
        retrieveWeatherForecast()
    }
    
    func retrieveWeatherForecast() {
        let forecastService = ForecastService(APIKey: openWeatherMapAPIKey)
        let date = Date().getTodaysDate()
        
        forecastService.getWeatherForecast(city: "Bratislava") { (forecast) in
            
            if let forecast = forecast?.weatherForecastArray{
                self.appDelegate.forecast = forecast
            }
            
            if let currentForecast = forecast?.weatherForecastArray.first {
                
                DispatchQueue.main.async {
                    if let temperature = currentForecast.temperature{
                        self.temperatureLabel.text = "\(temperature)°C"
                    }
                    if let humidity = currentForecast.humidity{
                        self.humidityLabel.text = "\(humidity)%"
                    }
                    if let pressure = currentForecast.pressure{
                        self.pressureLabel.text = "\(pressure)hPa"
                    }
                    if let image = currentForecast.icon{
                        self.weatherIcon.image = image
                    }
                    if let wind = currentForecast.windSpeed{
                        self.windSpeedLabel.text = "\(wind)m/s"
                    }
                    
                    self.dateLabel.text = date.date
                    self.dayLabel.text = date.day
                }
            }
            
        }
        

    }

}

