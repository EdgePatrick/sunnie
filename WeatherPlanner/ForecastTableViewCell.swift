//
//  ForecastTableViewCell.swift
//  WeatherPlanner
//
//  Created by Patrik Roh on 05/05/2017.
//  Copyright © 2017 Patrik Roh. All rights reserved.
//

import UIKit

class ForecastTableViewCell: UITableViewCell {

    @IBOutlet weak var day: UILabel!
    
    @IBOutlet weak var temperature: UILabel!
    
    @IBOutlet weak var icon: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
