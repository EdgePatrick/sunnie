//
//  ResultViewController.swift
//  WeatherPlanner
//
//  Created by Patrick Edge on 30.4.17.
//  Copyright © 2017 Patrik Roh. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var cityArray: [String] = []
    var forecastDict: [String:[WeatherForecast]] = [:]
    var topDaysDict: [String:[Int]] = [:]
    
    var sections: [Sections] = []
    
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: tableView setup
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Best days to visit \(cityArray[section]):"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "resultCell", for: indexPath) as! ResultTableViewCell
        
        cell.dayLabel.text = sections[indexPath.section].items[indexPath.row].detaileddate
        cell.temperatureLabel.text = "\(sections[indexPath.section].items[indexPath.row].temperature!)°C"
        cell.icon.image = sections[indexPath.section].items[indexPath.row].icon
        
        
        return cell
    }
    
    // MARK: view lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.sections = SectionsData().getSectionsData(cityArray: cityArray, forecastDict: forecastDict, topDaysDict: topDaysDict)
        
        tableView.backgroundColor = .clear
        
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width
        let height = bounds.size.height
        
        let background = Background(width: width, height: height)
        let layer = background.getBackgroundLayerBlue()
        view.layer.insertSublayer(layer, at: 0)
        
        tableView.delegate = self
        tableView.dataSource = self
    }


}
