//
//  CityScore.swift
//  WeatherPlanner
//
//  Created by Patrick Edge on 18.4.17.
//  Copyright © 2017 Patrik Roh. All rights reserved.
//

import Foundation

struct CityScore {
    
    //var score: Int?
    
    //private var weatherIdScore: Int?
    //private var temperatureScore: Int?
    
    
    init() {
        
        //self.weatherIdScore = computeWeatherIDScore(weatherId: forecast.weatherId!)
        //self.temperatureScore = computeTemperatureScore(temperature: forecast.temperature!)

        //self.score = computeWeatherIDScore(weatherId: forecast.weatherId!) + computeTemperatureScore(temperature: forecast.temperature!)
        
    }
    
    func compute (weatherArray: [WeatherForecast]) -> (score: [Int], indexes: [Int]) {
        var scoreArray: [Int] = []
        var arrayTotal = 0
        var aboveAverageScore: [Int] = []
        var indexes: [Int] = []
        
        //Vytvorenie scoreArray - ok
        for element in weatherArray {
            scoreArray.append(computeWeatherIDScore(weatherId: element.weatherId!) + computeTemperatureScore(temperature: element.temperature!))
        }
        //Výpočet Array total - ok
        for score in scoreArray {
            arrayTotal = arrayTotal + score
        }
        //Výpočet avg - ok 
        
        let avg = arrayTotal/scoreArray.count
        print("Average: \(avg)")
        
        //Vytvorenie aboveAverageScore - ok
        for element in scoreArray {
            if element > avg {
                aboveAverageScore.append(element)
            }
        }
        
        let unique = Array(Set(aboveAverageScore))
        
        //Vytvorenie indexes
        for element in unique {
            indexes.append(contentsOf: indexesOf(object: element, array: scoreArray))
        }
        
        //let indexes = scoreArray.indexesOf(object: 10)
        return (scoreArray, indexes.sorted())
    }
    
    func indexesOf(object: Int, array: [Int]) -> [Int]{
        var result: [Int] = []
        for (index, element) in array.enumerated() {
            if element == object{
                result.append(index)
            }
        }
        return result
    }
    
    func computeWeatherIDScore(weatherId: Int) -> Int {
        
        switch(weatherId){
            //Thunderstorm
        case 200,201,202,210,211,212,221,230,231,232:
            return 1
            //Drizzle
        case 300,301,302,310,311,312,313,314,321:
            return 2
            //Rain
        case 500,501,502,503,504,511,520,521,522,531:
            return 0
            //Light snow
        case 600,601:
            return 3
            //Heavy snow
        case 602,611,612,615,616,620,621,622:
            return 2
            //Atmosphere
        case 701,711,721,731,741,751,761,762,771:
            return 3
            //Tornado
        case 781,900:
            return 0
            //Clear
        case 800:
            return 10
            //Clouds
        case 801,802,803,804:
            return 7
            //Extreme
        case 901,902,906:
            return 0
            //Cold, Hot, Windy
        case 903,904,905:
            return 2
            //Calm, breeze
        case 951,952,953:
            return 5
            //Additional
        case 954,955,956:
            return 3
            //Gale, storm, hurricane
        case 957,958,959,960,961,961:
            return 0
        default: return 0
        }
        
    }
    
    func computeTemperatureScore(temperature: Int) -> Int {
        
        switch temperature {
        
        case -6 ... -4:
            return 1
        case -3...0:
            return 2
        case 1...3:
            return 3
        case 4...7:
            return 4
        case 8...10:
            return 5
        case 11...13:
            return 6
        case 14...17:
            return 7
        case 18...21:
            return 8
        case 21...23:
            return 9
        case 24...26:
            return 10
        case 27...29:
            return 9
        case 30...32:
            return 8
        case 33...35:
            return 7
        case 36...38:
            return 6
        case 39...41:
            return 5
        case 42...43:
            return 3
        case 44...46:
            return 2
        case 47...100:
            return 1
        default:
            return 0
        }
    }
    
    
}
